import { ReactComponent as Home } from '../../assets/svg/home.svg';
import { ReactComponent as DeviceTypes } from '../../assets/svg/device-types.svg';
import { ReactComponent as Devices } from '../../assets/svg/devices.svg';
import { ReactComponent as Location } from '../../assets/svg/location.svg';
import { ReactComponent as Measurement } from '../../assets/svg/measurement.svg';
import { ReactComponent as Network } from '../../assets/svg/network.svg';
import { ReactComponent as UnitTypes } from '../../assets/svg/unit-type.svg';
import { ReactComponent as Users } from '../../assets/svg/users.svg';
import { ReactComponent as SensorTypes } from '../../assets/svg/sensor-types.svg';
import { ReactComponent as Organisations } from '../../assets/svg/organisations.svg';
import { ReactComponent as Logout } from '../../assets/svg/logout.svg';
import Navbar from '../layout/nav-bar/navbar';
import styled from 'styled-components';
import { RouteItem } from '../shared/models';
import { Header } from '../layout';
import { Outlet } from 'react-router-dom';
/* eslint-disable-next-line */
export interface PageContainerProps {}

const routes: RouteItem[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: Home,
  },
  {
    name: 'Metingen',
    url: '/measurements',
    icon: Measurement,
  },
  {
    name: 'Apparaten',
    url: '/devices',
    icon: Devices,
  },
  {
    name: 'Sensoren',
    url: '/sensors',
    icon: SensorTypes,
  },
  {
    name: 'Locaties',
    url: '/locations',
    icon: Location,
  },
  {
    name: 'Organisaties',
    url: '/organisations',
    icon: Organisations,
  },
  {
    name: 'Gebruikers',
    url: '/users',
    icon: Users,
  },
  {
    name: 'Apparaat types',
    url: '/device-types',
    icon: DeviceTypes,
  },
  {
    name: 'Meet eenheden',
    url: '/unit-types',
    icon: UnitTypes,
  },
  {
    name: 'Bronnen',
    url: '/sources',
    icon: Network,
  },
  {
    name: 'Uitloggen',
    url: '/login',
    icon: Logout,
  },
];

interface ContainerProps {
  menuHoverd: boolean;
}

export function PageContainer(props: PageContainerProps) {
  const isHoveredOn = true;

  return (
    <FlexWrapper>
      <Header></Header>
      <RowWrapper>
        <Navbar routes={routes}></Navbar>
        <StyledPageContainer menuHoverd={isHoveredOn}>
          <Outlet />
        </StyledPageContainer>
      </RowWrapper>
    </FlexWrapper>
  );
}

const FlexWrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
`;
const RowWrapper = styled.div`
  display: flex;
  width: 100%;
  height: calc(100% - 140px);
  flex-direction: row;
`;

const StyledPageContainer = styled.div<ContainerProps>`
  width: ${(props) =>
    props.menuHoverd ? 'calc(100% - 220px)' : 'calc(100% - 90px)'};
  transition: width 0.3s ease;
  background-color: ${(props) => props.theme.backgroundSecondary};
  position: relative;
  overflow: hidden;
  color: ${(props) => props.theme.primaryDefault}
`;

export default PageContainer;
