export interface RouteItem {
  url: string;
  name: string;
  icon: React.FunctionComponent;
}