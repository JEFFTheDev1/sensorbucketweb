/* eslint-disable @typescript-eslint/no-explicit-any */
import styled from 'styled-components';
import { useTable, usePagination, Row } from 'react-table';
import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import { DateRangePicker } from 'react-date-range';
import { StrictMode } from 'react';
import 'reflect-metadata'
import { ReactComponent as Calendar } from '../../../assets/svg/calendar.svg';
import { Measurement, MeasurementService } from '../../api/measurement.service';

/* eslint-disable-next-line */
export interface PaginatedTableProps {}

export const PaginatedTable = (props: PaginatedTableProps) => {
  const [measurements, setMeasurements] = useState<Measurement[]>([])
  const [device, setDevice] = useState<any>("bestaatniet")

  useEffect(() => {
    fetchMeasurements()
  }, [])

  useEffect(() => {
    fetchMeasurements()
  }, [device])

  const fetchMeasurements = async () => {
    console.log("getting measurements")
    const res = await MeasurementService.getMeasurements(device)
    console.log(`[Paginated Table]: fetched measurements`, res)
    setMeasurements(res)
  }

  const columns = React.useMemo(
    () => [
      {
        Header: 'Metingen',
        columns: [
          {
            Header: 'Tijdstip',
            accessor: 'timestamp',
          },
          {
            Header: 'Waarde',
            accessor: 'value',
          },
          {
            Header: 'Meet eenheid',
            accessor: 'measurement_type_unit',
          },
          {
            Header: 'Organisatie',
            accessor: 'measurementOrganisationName',
          },
          {
            Header: 'Locatie',
            accessor: 'locationId',
          },
        ],
      },
    ],
    []
  );
  // eslint-disable-next-line react-hooks/exhaustive-deps
  // let data = React.useMemo(() => makeData(1000), []).sort(
  //   (a: any, b: any) => b.measurementTimestamp - a.measurementTimestamp
  // );
  // data = data.sort((e: any) => e.measurementValue);
  return (
    <StyledContainer>
      <Filters onDeviceChange={(dev) => setDevice(dev.value)} />
      <Table columns={columns} data={measurements} />
    </StyledContainer>
  );
}

function Filters({onDeviceChange}:{onDeviceChange?: (dev: any) => void}) {
  const devices = [
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048281_1", label: "70B3D57ED0048281_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048281_2", label: "70B3D57ED0048281_2"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED004843B_1", label: "70B3D57ED004843B_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED004843B_2", label: "70B3D57ED004843B_2"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED004843D_1", label: "70B3D57ED004843D_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED004843D_2", label: "70B3D57ED004843D_2"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED004843E_1", label: "70B3D57ED004843E_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED004843E_2", label: "70B3D57ED004843E_2"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048440_1", label: "70B3D57ED0048440_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048440_2", label: "70B3D57ED0048440_2"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048441_1", label: "70B3D57ED0048441_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048441_2", label: "70B3D57ED0048441_2"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048442_1", label: "70B3D57ED0048442_1"},
    { value: "srn:asset:mfm-waterlevel:sensor:70B3D57ED0048442_2", label: "70B3D57ED0048442_2"},
  ];
  const sensors = [
    { value: 'Sensor 1', label: 'Sensor 1' },
    { value: 'Sensor 2', label: 'Sensor 2' },
    { value: 'Sensor 3', label: 'Sensor 3' },
  ];
  const dateRange = {
    startDate: new Date(),
    endDate: new Date(),
    key: 'selection',
  };

  const getIcon = () => {
    const Icon = Calendar;
    return <Icon />;
  };
  const [selectedDevice, setSelectedDevice] = useState(devices[0]);
  const [selectedSensor, setSelectedSensor] = useState(sensors[0]);
  const [selectedDateRange, setSelectedDateRange] = useState(dateRange);
  const [dateRangePickerOpen, setDateRangePickerOpen] = useState(false);

  const changeDeviceFilter = (dev: any) => {
    setSelectedDevice(dev)
    onDeviceChange && onDeviceChange(dev)
  }

  return (
    <StyledRow>
      <DateRangePickerContainer $active={dateRangePickerOpen}>
        <DateRangePicker
          ranges={[selectedDateRange]}
          onChange={(ranges) =>
            setSelectedDateRange({
              startDate: ranges['selection'].startDate ?? new Date(),
              endDate: ranges['selection'].endDate ?? new Date(),
              key: 'selection',
            })
          }
        ></DateRangePicker>
      </DateRangePickerContainer>
      <InputContainer>
        <label>Tijdstip van</label>
        <label>{selectedDateRange.startDate.toDateString()}</label>
      </InputContainer>
      <IconWrapper
        $active={dateRangePickerOpen}
        onClick={() => setDateRangePickerOpen(!dateRangePickerOpen)}
      >
        {getIcon()}
      </IconWrapper>
      <InputContainer>
        <label>Tijdstip tot</label>
        <label>{selectedDateRange.endDate.toDateString()}</label>
      </InputContainer>
      <InputContainer2>
        <label>Apparaat</label>
        <Select
          options={devices}
          value={selectedDevice}
          onChange={(value: any) => changeDeviceFilter(value)}
        ></Select>
      </InputContainer2>
    </StyledRow>
  );
}

function Table({ columns, data }: { columns: any; data: any }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
    },
    usePagination
  );

  console.log('[Table] Got data: ', data)

  return (
    <>
      <Scrollable>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps()}>
                    {column.render('Header')}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row: Row<object>, i: any) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </Scrollable>
      <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '}
        <span>
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span>
          | Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0;
              gotoPage(page);
            }}
            style={{ width: '100px' }}
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[10, 20, 30, 40, 50].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

export default PaginatedTable;

const StyledContainer = styled.div`
  padding-left: 2rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.1rem 0.5em;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }

  .pagination {
    padding: 0.5rem 0.1rem;
  }
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 150px;
  margin-bottom: 20px;
  margin-right: 10px;
`;

const InputContainer2 = styled.div`
  display: flex;
  flex-direction: column;
  width: 450px;
  margin-bottom: 20px;
  margin-right: 10px;
`;

const DateRangePickerContainer = styled.div<{ $active: boolean }>`
  display: ${(props) => (props.$active ? 'flex' : 'none')};
  z-index: 999;
  position: ${(props) => (props.$active ? 'absolute' : 'relative')};
  margin-top: 60px;
  ${(props) => `background-color:${props.theme.primaryDefault};`}
  padding: 5px;
`;

const Scrollable = styled.div`
  overflow: scroll;
  max-height: 55vh;
`;

const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
`;

const IconWrapper = styled.div<{ $active: boolean }>`
  height: 50px;
  width: 30px;
  padding: 5px;
  margin-right: 10px;
  border-radius: 3px;
  ${(props) =>
    props.$active && `background-color:${props.theme.primaryDefault};`}
  display: flex;
  align-items: center;
  box-sizing: border-box;

  svg {
    fill: ${(props) => (props.$active ? '#fff;' : `${props.theme.fontColor}`)};
    height: auto;
    width: 1.1rem;
  }
`;
