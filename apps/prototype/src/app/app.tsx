// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.scss';
import StyleProvider from './layout/themes';
import RoutingContainer from './ronting-container/routingContainer';

export function App() {
  return (
    <StyleProvider>
      <RoutingContainer></RoutingContainer>
    </StyleProvider>
  );
}

export default App;
