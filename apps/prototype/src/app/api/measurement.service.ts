import axios, { AxiosInstance } from "axios";
import { plainToClass, Type } from "class-transformer";

// type QueryFilters struct {
// 	ThingURNs        []string
// 	LocationIDs      []int
// 	MeasurementTypes []string
// }

export class Measurement {
    timestamp!: string;
    thing_urn!: string;
    value!: number;
    measurement_type!: string;
    measurement_type_unit!: string;
    location_id!: number;
    coordinates!: number[];
    metadata!: string;
}

class paginated {
    data!: Measurement[]
}

const axiosConfig = {
    headers: {
      'Content-Type': 'application/json',
    }
  }

class _MeasurementService {
    private readonly r: AxiosInstance;

    constructor(baseURL: string) {
        this.r = axios.create({
            baseURL: baseURL,
        });
    }

    async getMeasurements(dev: any): Promise<Measurement[]> {
        const res = await this.r.get<paginated>(`/2022-01-01T00:00:00Z/2022-12-31T23:59:59Z?thing_urn=${encodeURIComponent(dev)}`, axiosConfig);
        const data = res.data.data.map((meas) => plainToClass(Measurement, meas));
        return data;
    }
}

export const MeasurementService = new _MeasurementService("http://localhost:3001/");