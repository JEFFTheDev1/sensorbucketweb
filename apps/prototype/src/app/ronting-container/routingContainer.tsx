import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import styled from 'styled-components';
import Dashboard from '../dashboard/dashboard';
import DeviceTypes from '../device-types/deviceTypes';
import Devices from '../devices/devices';
import Locations from '../locations/locations';
import LoginPage from '../login-page/loginPage';
import Measurements from '../measurements/measurements';
import Sources from '../sources/sources';
import Organizations from '../organizations/organizations';
import PageContainer from '../page-container/pageContainer';
import SensorTypes from '../sensor-types/sensorTypes';
import UnitTypes from '../unit-types/unitTypes';
import Users from '../users/users';

/* eslint-disable-next-line */
export interface RontingContainerProps {}

const ViewPortContainer = styled.div`
  background-color: ${(props) => props.theme.backgroundDefault};
  height: 100vh;
  display: flex;

  *::-webkit-scrollbar {
    width: 4px;
  }

  *::-webkit-scrollbar-track {
    background: ${(props) => props.theme.lightGrey};
  }

  *::-webkit-scrollbar-thumb {
    background: ${(props) => props.theme.primaryDefault};
    box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.3);
    opacity: 0.5;
  }
`;

const AppContent = styled.div`
  width: 100%;
  transition: width 0.3s ease;
  background-color: ${(props) => props.theme.backgroundSecondary};
  position: relative;
  overflow: hidden;
  display: flex;
`;

export function RontingContainer(props: RontingContainerProps) {
  return (
    <ViewPortContainer>
      <AppContent>
        <Router>
          <Routes>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/" element={<PageContainer />}>
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/organisations" element={<Organizations />} />
              <Route path="/users" element={<Users />} />
              <Route path="/locations" element={<Locations />} />
              <Route path="/device-types" element={<DeviceTypes />} />
              <Route path="/devices" element={<Devices />} />
              <Route path="/unit-types" element={<UnitTypes />} />
              <Route path="/measurements" element={<Measurements />} />
              <Route path="/sensors" element={<SensorTypes />} />
              <Route path="/sources" element={<Sources />} />
            </Route>
          </Routes>
        </Router>
      </AppContent>
    </ViewPortContainer>
  );
}

export default RontingContainer;
