/* eslint-disable @typescript-eslint/no-explicit-any */
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';
import logo from '../../assets/svg/logo.svg';

/* eslint-disable-next-line */
export interface LoginPageProps {}

export function LoginPage(props: LoginPageProps) {
  const navigate = useNavigate();
  const handleSubmit = (event: any) => {
    event.preventDefault();
    navigate('/dashboard', { replace: true });
  };

  return (
    <LoginPageContainer>
      <LoginFormContainer>
        <LogoContainer>
          <img src={logo} width="200" height="80" alt="logo"></img>
        </LogoContainer>
        <LoginForm onSubmit={handleSubmit}>
          <InputContainer>
            <label>Gebruikersnaam</label>
            <input type="text" name="username" />
          </InputContainer>
          <InputContainer>
            <label>Wachtwoord</label>
            <input type="password" name="password" />
          </InputContainer>
          <InputContainer>
            <label>Organisatie</label>
            <select>
              <option selected value="grapefruit">Organisatie 1</option>
              <option value="lime">Organisatie 2</option>
              <option value="mango">Organisatie 3</option>
            </select>
          </InputContainer>

          <ButtonWrapper>
            <input type="submit" value="Inloggen" />
          </ButtonWrapper>
        </LoginForm>
      </LoginFormContainer>
    </LoginPageContainer>
  );
}

export default LoginPage;

const LoginPageContainer = styled.div`
  background-color: ${(props) => props.theme.backgroundDefault};
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;
`;

const LoginFormContainer = styled.div`
  color: ${(props) => props.theme.backgroundTernary};
  background-color: ${(props) => props.theme.backgroundSecondary};
  display: flex;
  width: 300px;
  height: 470px;
  flex-direction: column;
  overflow: hidden;
`;

const LoginForm = styled.form`
  flex: 2;
  display: flex;
  flex-direction: column;
  justify-content: start;
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: end;
  align-self: center;
  margin-top: 1em;
  input {
    font-size: 18px;
    font-family: 'DINWebPro', sans-serif;
    color: #485061;
  }
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  gap: 8px;
  margin: 10px;
  width: 200px;
`;
