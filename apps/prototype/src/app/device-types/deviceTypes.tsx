import styled from 'styled-components';

/* eslint-disable-next-line */
export interface DeviceTypesProps {}

export function DeviceTypes(props: DeviceTypesProps) {
  return (
    <StyledContainer>
      <h1>Apparaat types</h1>
    </StyledContainer>
  );
}

export default DeviceTypes;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;
