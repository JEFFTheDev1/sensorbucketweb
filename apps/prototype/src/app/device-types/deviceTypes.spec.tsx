import { render } from '@testing-library/react';

import DeviceTypes from './deviceTypes';

describe('DeviceTypes', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DeviceTypes />);
    expect(baseElement).toBeTruthy();
  });
});
