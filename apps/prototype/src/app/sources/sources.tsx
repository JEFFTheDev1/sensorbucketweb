import styled from 'styled-components';

/* eslint-disable-next-line */
export interface SourcesProps {}

export function Sources(props: SourcesProps) {
  return (
    <StyledContainer>
      <h1>Bronnen</h1>
    </StyledContainer>
  );
}

export default Sources;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;