import styled from 'styled-components';

/* eslint-disable-next-line */
export interface DevicesProps {}

export function Devices(props: DevicesProps) {
  return (
    <StyledContainer>
      <h1>Apparaten</h1>
    </StyledContainer>
  );
}

export default Devices;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;