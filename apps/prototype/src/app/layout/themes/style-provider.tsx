/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react';
import { DefaultTheme, ThemeProvider } from 'styled-components';

import { darkTheme, lightTheme } from './theme';

export interface StyleProviderContextData {
  setTheme: (theme: string) => void;
  getTheme: () => string;
  theme: DefaultTheme;
}

export const StyleProviderContext =
  React.createContext<StyleProviderContextData>({
    setTheme: () => {},
    getTheme: () => {
      return 'light';
    },
    theme: lightTheme,
  });

const StyleProvider = ({ children }: { children: any }) => {
  const [themeObj, setThemeObj] = React.useState<DefaultTheme>(lightTheme);
  const [internalTheme, setInternalTheme] = React.useState('light');

  React.useEffect(() => {
    if (internalTheme === 'light') {
      setThemeObj(lightTheme);
    } else {
      setThemeObj(darkTheme);
    }
  }, [internalTheme]);

  React.useEffect(() => {
    const theme = getTheme();
    setInternalTheme(theme);
  }, []);

  const getTheme = () => {
    const theme = window.localStorage.getItem('sb-theme');
    if (theme) return theme;
    return 'light';
  };

  const setTheme = (theme: string) => {
    window.localStorage.setItem('sb-theme', theme);
    setInternalTheme(theme);
  };

  return (
    <StyleProviderContext.Provider
      value={{
        getTheme: getTheme,
        setTheme: setTheme,
        theme: themeObj,
      }}
    >
      <ThemeProvider theme={themeObj}>{children}</ThemeProvider>
    </StyleProviderContext.Provider>
  );
};

export default StyleProvider;
