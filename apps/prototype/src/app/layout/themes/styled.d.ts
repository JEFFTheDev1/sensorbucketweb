// import original module declarations
import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    id: string;
    backgroundDefault: string;
    backgroundSecondary: string;
    backgroundTernary: string;
    primaryDefault: string;
    primaryDefaultOpaque: string;
    darkGrey: string;
    mediumGrey: string;
    lightGrey: string;
    errorRed: string;
    successGreen: string;
    warningOrange: string;
    fontColor: string;
    inputBorder: string;
    inputBackground: string;
  }
}
