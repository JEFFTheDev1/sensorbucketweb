import { DefaultTheme } from 'styled-components';

export const darkTheme: DefaultTheme = {
  id: 'sb-dark',
  backgroundDefault: '#2d333f',
  backgroundSecondary: '#343A45',
  backgroundTernary: '#485061',
  primaryDefault: '#105cf6',
  primaryDefaultOpaque: '#105cf61f',
  darkGrey: '#666666',
  mediumGrey: '#b8beca',
  lightGrey: '#f2f2f2',
  errorRed: '#cc0033',
  successGreen: '#0ea30b',
  warningOrange: '#ffa050',
  fontColor: '#dbdfe6',
  inputBorder: '2px solid #596272',
  inputBackground: '#fff',
};

export const lightTheme: DefaultTheme = {
  id: 'sb-light',
  backgroundDefault: '#eff1f4',
  backgroundSecondary: '#fff',
  backgroundTernary: '#5c748d',
  primaryDefault: '#c2004b',
  primaryDefaultOpaque: '#009dff1a',
  darkGrey: '#666666',
  mediumGrey: '#b8beca',
  lightGrey: '#f2f2f2',
  errorRed: '#cc0033',
  successGreen: '#0ea30b',
  warningOrange: '#ffa050',
  fontColor: '#3D4853',
  inputBorder: '1px solid #D4D4D4',
  inputBackground: '#fff',
};

export const getTheme = (): DefaultTheme => {
  const theme = window.localStorage.getItem('sb-theme');
  if (theme) {
    return theme === 'light' ? lightTheme : darkTheme;
  }
  return lightTheme;
};
