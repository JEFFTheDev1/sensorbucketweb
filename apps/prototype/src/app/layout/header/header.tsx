import styled from 'styled-components';
import logo from '../../../assets/svg/logo.svg';

/* eslint-disable-next-line */
export interface HeaderProps {}

export function Header(props: HeaderProps) {
  return (
    <>
      <HeaderContainer>
        <img src={logo} width="200" height="80" alt="logo"></img>
      </HeaderContainer>
      <SubHeaderContainer>
        <h1>Sensor Bucket</h1>
      </SubHeaderContainer>
    </>
  );
}

const HeaderContainer = styled.div`
  display: flex;
  height: 80px;
  width: 100%;
  padding-left: 20px;
`;
const SubHeaderContainer = styled.div`
  display: flex;
  height: 60px;
  background-color: ${(props) => props.theme.primaryDefault};
  width: 100%;
  padding-left: 15px;
  h1 {
    font-family: pfdinstencil;
    color: white;
    margin-top: 15px;
  }
`;

export default Header;
