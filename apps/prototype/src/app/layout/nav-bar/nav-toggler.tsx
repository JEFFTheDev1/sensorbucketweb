import styled from 'styled-components';
import { ReactComponent as Arrow } from '../../../assets/svg/caret-down-solid.svg';

const StyledNavToggler = styled.div<{ $isHovering: boolean; $isOpen: boolean }>`
  position: absolute;
  bottom: 1rem;

  right: ${(props) => (props.$isHovering || props.$isOpen ? '-30px' : '-25px')};
  width: 30px;
  height: 40px;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 999;
  box-shadow: 4px 1px 5px -3px rgb(0 0 0 / 12%);
  opacity: ${(props) => (props.$isHovering || props.$isOpen ? '1' : '0')};
  background: ${(props) => props.theme.backgroundDefault};
  transition: all 0.2s ease;
  &:hover {
    cursor: pointer;
  }

  & svg {
    transform: rotate(${(props) => (props.$isOpen ? '90deg' : '-90deg')});
  }
`;

const StyledArrow = styled(Arrow)`
  height: 20px;
  width: 20px;
  fill: ${(props) => props.theme.primaryDefault};
  transition: all 0.1s ease;
`;

const NavToggler = ({
  isOpen,
  isHovering,
  onToggle,
}: {
  isOpen: boolean;
  isHovering: boolean;
  onToggle: () => void;
}) => {
  return (
    <StyledNavToggler
      $isHovering={isHovering}
      $isOpen={isOpen}
      onClick={() => onToggle()}
    >
      <StyledArrow></StyledArrow>
    </StyledNavToggler>
  );
};

export default NavToggler;
