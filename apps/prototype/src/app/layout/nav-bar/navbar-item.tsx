import React from 'react';
import styled from 'styled-components';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';

/* eslint-disable-next-line */
export interface NavbarItemProps {
  showText: boolean;
  route: RouteItem;
  active?: boolean;
}

export interface RouteItem {
  url: string;
  name: string;
  icon: React.FunctionComponent;
}

interface NavLinkTextProps {
  showText: boolean;
}

const StyledLink = styled(Link)`
  display: flex;
  height: 50px;
  justify-content: space-between;
  align-items: center;
  transition: margin-left 0.2s ease, opacity 0.2s ease, color 0.3s ease;
  color: ${(props) => props.theme.fontColor};
  text-decoration: none;
`;

const StyledFakeLink = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  transition: margin-left 0.2s ease, opacity 0.2s ease, color 0.3s ease;
  color: ${(props) => props.theme.fontColor};
  text-decoration: none;

  &:hover {
    cursor: pointer;
  }
`;

const NavLinkText = styled.p<NavLinkTextProps>`
  transition: all 0.3s ease;
  transition: margin-left 0.2s ease, opacity 0.2s ease, color 0.3s ease;
  opacity: ${(props) => (props.showText ? '1' : '0')};
  margin-left: ${(props) => (props.showText ? '10px' : '50px')};
  text-align: left;
`;

const IconWrapper = styled.div<{ $active: boolean }>`
  padding: 5px;
  /* margin-left: -5px; */
  border-radius: 3px;
  ${(props) => props.$active && `background-color:${props.theme.primaryDefault};`}
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;

  svg {
    fill: ${(props) => (props.$active ? '#fff;' : `${props.theme.mediumGrey}`)};
    height: auto;
    width: 1.1rem;
  }
`;

export function NavbarItem({ route, showText }: NavbarItemProps) {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const resolved = route.url ? useResolvedPath(route.url) : null;
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const match = resolved ? useMatch({ path: resolved?.pathname, end: false }) : null;

  const getIcon = () => {
    const Icon = route.icon;
    return <Icon />;
  };

  return route.url ? (
    <StyledLink to={route.url}>
      <IconWrapper $active={match !== null}>{getIcon()}</IconWrapper>
      <NavLinkText showText={showText}>{route.name}</NavLinkText>
    </StyledLink>
  ) : (
    <StyledFakeLink>
      <IconWrapper $active={false}>{getIcon()}</IconWrapper>
      <NavLinkText showText={showText}>{route.name}</NavLinkText>
    </StyledFakeLink>
  );
}
