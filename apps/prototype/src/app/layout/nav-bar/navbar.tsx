import { useState } from 'react';
import styled from 'styled-components';
import { RouteItem } from '../../shared/models';
import NavToggler from './nav-toggler';
import { NavbarItem } from './navbar-item';

export interface NavbarProps {
  routes: RouteItem[];
}

interface NavProps {
  hovered: boolean;
}

const NavWrapper = styled.div<NavProps>`
  position: relative;
  height: 100%;
  width: ${(props) => (props.hovered ? '230px' : '90px')};
  transition: width 0.3s ease;
`;

const Nav = styled.nav`
  height: 100%;
  background-color: ${(props) => props.theme.backgroundDefault};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 25px;
  overflow: hidden;
  box-sizing: border-box;
  box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.1);
  position: relative;
`;

const NavContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
`;

export function NavBar({ routes = [] }: NavbarProps) {
  const [hoveringNavbar, setHoveringNavbar] = useState(false);
  const [isHoveredOn, setIsHoveredOn] = useState(true);
  
  return (
    <NavWrapper
      hovered={isHoveredOn}
      onMouseEnter={() => {
        setHoveringNavbar(true);
      }}
      onMouseLeave={() => {
        setHoveringNavbar(false);
      }}
    >
      <NavToggler isOpen={isHoveredOn} isHovering={hoveringNavbar} onToggle={() => {
        setIsHoveredOn(!isHoveredOn);
      }}></NavToggler>
      <Nav>
        <NavContainer>
          {routes.map((route) => {
            return <NavbarItem key={route.name} route={route} showText={isHoveredOn}></NavbarItem>;
          })}
        </NavContainer>
      </Nav>
    </NavWrapper>
  );
}

export default NavBar;
