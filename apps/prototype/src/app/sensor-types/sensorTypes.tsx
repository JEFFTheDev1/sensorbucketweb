import styled from 'styled-components';

/* eslint-disable-next-line */
export interface SensorTypesProps {}

export function SensorTypes(props: SensorTypesProps) {
  return (
    <StyledContainer>
      <h1>Sensor types</h1>
    </StyledContainer>
  );
}

export default SensorTypes;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;