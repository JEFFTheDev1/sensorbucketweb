import styled from 'styled-components';
import { PaginatedTable } from '../shared/components';

/* eslint-disable-next-line */
export interface MeasurementsProps {}

export function Measurements(props: MeasurementsProps) {
  return (
    <StyledContainer>
      <PaginatedTable></PaginatedTable>
    </StyledContainer>
  );
}

export default Measurements;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;