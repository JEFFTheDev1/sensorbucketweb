import styled from 'styled-components';

/* eslint-disable-next-line */
export interface OrganizationsProps {}

export function Organizations(props: OrganizationsProps) {
  return (
    <StyledContainer>
      <h1>Organisaties</h1>
    </StyledContainer>
  );
}

export default Organizations;

const StyledContainer = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
`;